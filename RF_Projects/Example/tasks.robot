
*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${LOGIN URL}      https://192.168.11.30:2443/swp/customgroup/saa
${BROWSER}        Firefox
${username}       manu
${password}       Venus2009Venus2009+
${title_akura}    saa75_akura
${title_tourch}   saa75_torch
*** Test Cases ***
Valid Login
    Open Browser To Login Page
    Input Username
    Input Password
    Select Akura
    Login
    Logout
    # Welcome Page Should Be Open
    # [Teardown]    Close Browser



*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Sleep   2s 
    Click Element   //*[contains(@class,"auth_OL") ]
    Sleep   2s
Input Username
    # [Arguments]    ${username}
    Input Text    //*[contains(@id,"gwt-debug-platform_login-username") ]    ${username}
    Sleep   2s

Input Password
    # [Arguments]    ${password}
    Input Text     //*[contains(@id,"gwt-debug-platform_login-password") ]    ${password}
    Sleep   2s

Select Akura
    # [Arguments]    ${password}
    Sleep   2s
    Click Element   //*[contains(@class,'auth_NUC')]
    Sleep   2s
    Click Element   //*[contains(@class,'auth_DCC auth_FDC auth_GCC auth_ADC auth_CTC')]

Login
    Sleep   4s
    Click Button    gwt-debug-platform_login-logon

# Logout
#     Sleep   2s
#     Click Element   class:swp_IN
    

# Welcome Page Should Be Open
#     Title Should Be    Welcome Page