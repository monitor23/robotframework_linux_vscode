# Robot Framework Repository

## Directory Tree

    RobotFreamework_Linux_VsCode
    ├── Docs
    │   └── Notes.md
    ├── geckodriver.log
    ├── ReadMe.md
    └── RF_Projects
        └── Example
            ├── conda.yaml
            ├── output
            │   ├── geckodriver-1.log ... 59.log
            │   ├── log.html
            │   ├── output.xml
            │   ├── selenium-screenshot-1.png ... 10.png
            │   ├── stderr.log
            │   └── stdout.log
            ├── robot.yaml
            └── tasks.robot
