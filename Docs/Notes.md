# Basic Installation Guide

## Installing VS Code on Ubuntu

    To install vs code we should follow these steps:
        1. GOTO https://code.visualstudio.com/download
        2. Download .deb 64bit installer
        3. Open Terminal Change Directory to Download by typing cd Desktop/
        4. Now type sudo apt install ./<file_name>.deb
        5. VS Code is now Installed

## Installing Plugins

    Once VS Code is installed follow these steps:
        1. Run VS Code from Menu or type Code . in Terminal
        2. On the left Toolbar of VS code select option Number 4
        3. When Market Place is loaded search Robocorp
        4. Install both plugins Robocorp Code and Robot Framework Language Server

## Installing Web Drivers

    Once VS Code and RF plugins are installed one should follow these steps:
        1. Open a terminal and type following: sudo apt-get install firefox-geckodriver chromium-Chromedriver
        2. Webdrivers for Both Firefox and Chrome are now installed

## Creating A New Project

    To create a new robot project one must follow these steps:
        1. Open Vs Code
        2. Press Ctrl/Cmd + Shift + P keys
        3. Select create a robot from the menu
        4. Name the project
        5. New Project is created